# Concrete5ContactExpress

FIXME: on field!!!

[![Concrete5ContactExpress](https://radeff.red/pics/git/concrete5_contact_express.jpg)](Concrete5ContactExpress)

## Français
un package concrete5 pour importer vos contacts (carnet d'adresses au format vcf/csv) dans un module express

# Instructions pour l'installation
- Installer le package sous votre répertoire /packages
- Chercher dans votre interface d'administration "Améliorer concrete5"
- Chercher le packages _"Concrete5ContactExpress"_
- Installer le package

Vous verrez ensuite dans votre dashboard l'entrée "ContactExpress"
---

## English:
a concrete5 package to import a vcf/csv adressbook into concrete5's express

# Installation Instructions
- Install package in your site's /packages directory
- Go to "Extend concrete5 > Add Functionality"
- Activate package _"Concrete5ContactExpress"_

You will then see in your dashboard the entry "ContactExpress"
